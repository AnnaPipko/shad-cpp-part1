#pragma once

#include <stdexcept>

template<class Iterator, class Predicate>
Iterator RemoveIf(Iterator first, Iterator last, Predicate pred) {
    int erase_count = 0;
    while (first != last && !pred(*first)){
        ++first;
    }
    auto it_erase = first;
    while (first != last) {
        ++erase_count;
        auto second = first;
        ++second;
        while (second != last && !pred(*second)) {
            ++second;
        }
        auto it = first;
        ++it;
        for (; it != second; ++it) {
            *(it_erase) = *it;
            ++it_erase;
        }
        first = second;
    }
    return it_erase;
}

