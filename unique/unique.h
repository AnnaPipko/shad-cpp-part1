#pragma once

#include <vector>
#include <stdexcept>

std::vector<int> Unique(const std::vector<int>& data) {
    std::vector<int> res;
    if (data.size() == 0){
        return res;
    }
    int el = data[0];
    res.push_back(el);
    for (int i = 1; i < data.size(); ++i){
        if (data[i] != el){
            el = data[i];
            res.push_back(el);
        }

    }
    return res;
}
