#pragma once

#include <stdexcept>
#include <cmath>

enum RootsCount {
    ZERO,
    ONE,
    TWO,
    INF
};

struct Roots {
    RootsCount count;
    double first, second;
    Roots(RootsCount rc, double fr, double sr): count(rc), first(fr), second(sr) {};
};

Roots SolveQuadratic(int a, int b, int c) {
    if (a==0){
        if (b==0){
            if (c==0){
                return Roots(INF, 0, 0);
            }
            return Roots(ZERO, 0, 0);
        }
        float x = -c;
        x /= b;
        return Roots(ONE, x, 0);
    }
    float discr = b * b - 4 * a * c;
    if (discr < 0){
        return Roots(ZERO, 0, 0);
    }
    if (discr == 0){
        float x = -0.5 * b;
        x /= a;
        return Roots(ONE, x, 0);
    }
    if (a > 0) {
        float first = (-b - std::sqrt(discr)) / 2 / a;
        float second = (-b + std::sqrt(discr)) / 2 / a;
        return Roots(TWO, first, second);
    } else {
        float second = (-b - std::sqrt(discr)) / 2 / a;
        float first = (-b + std::sqrt(discr)) / 2 / a;
        return Roots(TWO, first, second);
    }
}
