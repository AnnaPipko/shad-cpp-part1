#pragma once

#include <iostream>

template <typename T>
class List {
public:

    class NodeBase {
    public:
        NodeBase() : next_(this), prev_(this) {}
        virtual ~NodeBase() {}
        NodeBase* next_;
        NodeBase* prev_;
        friend class List;
    };

    class ListNode : public NodeBase {
    public:
        explicit ListNode(const T& value) : NodeBase(), value_(value) {};
        explicit ListNode(T&& value) : NodeBase(), value_(std::move(value)) {};
        ~ListNode() override {};

        T& GetValue() {
            return value_;
        };
    private:
        T value_;
    };

    class Iterator {
    public:
        Iterator(NodeBase *current): current_(current) {}
        Iterator() {}

        Iterator& operator++() {
            current_ = current_->next_;
            return *this;
        }
        Iterator operator++(int) {
            Iterator temp(current_);
            current_ = current_->next_;
            return temp;
        }

        Iterator& operator--(){
            current_ = current_->prev_;
            return *this;
        }
        Iterator operator--(int) {
            Iterator temp(current_);
            current_ = current_->prev_;
            return temp;
        }

        T& operator*() const {
            return static_cast<ListNode*>(current_)->GetValue();
        };
        T* operator->() const {
            return &(static_cast<ListNode*>(current_)->GetValue());
        };

        bool operator==(const Iterator& rhs) const {
            return current_ == rhs.current_;
        }
        bool operator!=(const Iterator& rhs) const {
            return current_ != rhs.current_;
        }

    private:
        NodeBase *current_;
    };

    List() {}
    List(const List& other) {
        for (auto node : other){
            PushBack(node);
        }
    }
    List(List&& other) {
        dummy_.next_ = other.dummy_.next_;
        dummy_.prev_ = other.dummy_.prev_;
        dummy_.next_->prev_ = &dummy_;
        dummy_.prev_->next_ = &dummy_;
        other.dummy_.prev_ = &other.dummy_;
        other.dummy_.next_ = &other.dummy_;
    }
    ~List() {
        auto ptr = dummy_.next_;
        while (ptr != &dummy_){
            Unlink(static_cast<ListNode*>(ptr));
            ptr = dummy_.next_;
        }
    }

    List& operator=(const List& other) {
        if (!IsEmpty()){
            auto ptr = dummy_.next_;
            while (ptr != &dummy_){
                Unlink(static_cast<ListNode*>(ptr));
                ptr = dummy_.next_;
            }
        }
        for (auto node : other){
            PushBack(node);
        }
        return *this;
    }
    List& operator=(List&& other) {
        if (!IsEmpty()){
            auto ptr = dummy_.next_;
            while (ptr != &dummy_){
                Unlink(static_cast<ListNode*>(ptr));
                ptr = dummy_.next_;
            }
        }
        dummy_.next_ = other.dummy_.next_;
        dummy_.prev_ = other.dummy_.prev_;
        dummy_.next_->prev_ = &dummy_;
        dummy_.prev_->next_ = &dummy_;
        other.dummy_.prev_ = &other.dummy_;
        other.dummy_.next_ = &other.dummy_;
        return *this;
    }

    bool IsEmpty() const {
        return dummy_.next_ == &dummy_;
    }
    size_t Size() const {
        if (IsEmpty()) {
            return 0;
        }
        size_t list_size = 0;
        auto ptr = dummy_.next_;
        while (ptr != &dummy_){
            ++list_size;
            ptr = ptr->next_;
        }
        return list_size;
    };

    void Unlink(ListNode* node) {
        node->prev_->next_ = node->next_;
        node->next_->prev_ = node->prev_;
        delete node;
    }

    void LinkAfter(NodeBase* target, NodeBase* after) {
        after->prev_ = target;
        after->next_ = target->next_;
        target->next_ = after;
        after->next_->prev_ = after;
    }

    void PushBack(const T& elem) {
        auto node = new ListNode(elem);
        LinkAfter(dummy_.prev_, node);
    }
    void PushBack(T&& elem) {
        auto node = new ListNode(std::move(elem));
        LinkAfter(dummy_.prev_, node);
    }
    void PushFront(const T& elem) {
        auto node = new ListNode(elem);
        LinkAfter(&dummy_, node);
    }
    void PushFront(T&& elem) {
        auto node = new ListNode(std::move(elem));
        LinkAfter(&dummy_, node);
    }

    T& Front() {
        return static_cast<ListNode*>(dummy_.next_)->GetValue();
    }
    const T& Front() const {
        return static_cast<ListNode*>(dummy_.next_)->GetValue();
    }
    T& Back() {
        return static_cast<ListNode*>(dummy_.prev_)->GetValue();
    }
    const T& Back() const {
        return static_cast<ListNode*>(dummy_.prev_)->GetValue();
    }

    void PopBack() {
        Unlink(static_cast<ListNode*>(dummy_.prev_));
    }
    void PopFront() {
        Unlink(static_cast<ListNode*>(dummy_.next_));
    }

    Iterator Begin() {
        return Iterator(dummy_.next_);
    }
    Iterator End() {
        return Iterator(&dummy_);
    }

    const Iterator Begin() const {
        return Iterator(dummy_.next_);
    }
    const Iterator End() const {
        return Iterator(const_cast<NodeBase*>(&dummy_));
    }

private:
    NodeBase dummy_;
};

template <typename T>
typename List<T>::Iterator begin(List<T>& list) {
    return list.Begin();
}

template <typename T>
typename List<T>::Iterator end(List<T>& list) {
    return list.End();
}

template <typename T>
typename List<T>::Iterator begin(const List<T>& list) {
    return list.Begin();
}

template <typename T>
typename List<T>::Iterator end(const List<T>& list) {
    return list.End();
}
