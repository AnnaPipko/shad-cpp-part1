#include <testing.h>
#include <permutations.h>
#include <vector>
#include <algorithm>

using Permutations = std::vector<std::vector<int>>;

namespace tests {

void Small() {
    {
        Permutations expected{
            {0, 1, 2},
            {0, 2, 1},
            {1, 0, 2},
            {1, 2, 0},
            {2, 0, 1},
            {2, 1, 0}
        };
        ASSERT_EQ(expected, GeneratePermutations(3));
    }
    {
        Permutations expected{{0}};
        ASSERT_EQ(expected, GeneratePermutations(1));
    }
    {
        Permutations expected{{0, 1}, {1, 0}};
        ASSERT_EQ(expected, GeneratePermutations(2));
    }
}

void Big() {
    auto result = GeneratePermutations(8);
    std::vector<int> expected(8);
    for (int i = 0; i < 8; ++i)
        expected[i] = i;
    int i = 0;
    do {
        ASSERT_EQ(expected, result[i]);
        ++i;
    } while (std::next_permutation(expected.begin(), expected.end()));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Small);
    RUN_TEST(Big);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
