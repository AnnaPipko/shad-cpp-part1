#pragma once

#include <vector>
#include <stdexcept>

std::vector<std::vector<int>> GeneratePermutations(size_t len) {
    std::vector<std::vector<int>> res;
    std::vector<int> el(len, 0);
    size_t factorial = 1;
    for (int i = 2; i <= len; ++i){
        factorial *= i;
    }
    for (size_t i = 0; i < factorial; ++i) {
        el.back()++;
        for (auto k = len - 1; k > 0; --k){
            if (el[k] == dims[nd - k]){
                idx[k] = 0;
                idx[k - 1]++;
            }
        }
    }
    return res;
}
