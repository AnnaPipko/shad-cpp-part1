#pragma once

#include <vector>
#include <algorithm>

class RingBuffer {
public:
    explicit RingBuffer(size_t capacity) : capacity_(capacity), first_(capacity), last_(capacity-1) {
        data_.resize(capacity);
    }

    size_t Size() const {

        if (first_ < last_) {
            return last_ - first_ + 1;
        }
        return 0;
    }

    bool Empty() const {
        return first_ > last_;
    }

    bool TryPush(int element) {
        if (Size() == capacity_){
            return false;
        }
        ++last_;
        data_[last_ % capacity_] = element;
        if (first_ > capacity_){
            first_ -= capacity_;
            last_ -= capacity_;
        }
        return true;
    }

    bool TryPop(int *element) {
        if (Empty()){
            return false;
        }
        *element = data_[first_ % capacity_];
        ++first_;
        return true;
    }

private:
    std::vector<int> data_;
    size_t capacity_;
    size_t first_;
    size_t last_;
};

