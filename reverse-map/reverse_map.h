#pragma once

#include <string>
#include <map>
#include <stdexcept>

std::map<int, std::string> ReverseMap(const std::map<std::string, int>& map) {
    std::map<int, std::string> res;
    for (auto it = map.begin(); it != map.end(); ++it){
        res[it->second] = it->first;
    }
    return res;
}
