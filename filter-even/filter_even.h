#pragma once

#include <vector>
#include <stdexcept>

void FilterEven(std::vector<int> *data) {
    int erase_count = 0;
    auto first_pointer = data->begin();
    while (first_pointer != data->end() && *first_pointer % 2 == 0){
        ++first_pointer;
    }
    while (first_pointer != data->end()) {
        ++erase_count;
        auto second_pointer = first_pointer;
        ++second_pointer;
        while (second_pointer != data->end() && *second_pointer % 2 == 0) {
            ++second_pointer;
        }
        for (auto it = first_pointer + 1; it < second_pointer; ++it) {
            *(it - erase_count) = *it;
        }
        first_pointer = second_pointer;
    }
    for (int i = 0; i < erase_count; ++i){
        data->pop_back();
    }
}
