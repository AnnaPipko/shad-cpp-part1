#pragma once

#include <vector>

struct Sum {
    int operator()(int first, int second) {
        return first + second;
    }
};

struct Prod {
    int operator()(int first, int second) {
        return first * second;
    }
};

struct Concat {
    template<class T>
    std::vector<T> operator()(std::vector<T> first, std::vector<T>& second) {
        first.insert(first.end(), second.begin(), second.end());
        return first;
    }

};

template<class Iterator, class T, class BinaryOp>
T Fold(Iterator first, Iterator last, T init, BinaryOp func) {
    while (first != last) {
        init = func(init, *first);
        ++first;
    }
    return init;
}

class Length {
public:
    Length(int *len) : len_(len) {};
    template<class T>
    T operator()(T& first, T& second) {
        (*len_)++;
        return first;
    }

private:
    int *len_;
};

