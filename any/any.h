#pragma once

#include <memory>

class Any {
public:
    Any() {}

    template<class T>
    Any(const T& value) {
        inner_ = std::make_unique<Inner<T>>(value);
    }

    template<class T>
    Any& operator=(const T& value) {
        inner_.reset(new Inner<T>(value));
        return *this;
    }

    Any(const Any& rhs) : inner_(rhs.inner_->Clone()){}

    Any& operator=(const Any& rhs) {
        Any temp(rhs);
        Swap(temp);
        return *this;
    }

    ~Any() {}

    bool Empty() const {
        if (inner_.get() == nullptr) {
            return true;
        }
        return false;
    }

    void Clear() {
        inner_.reset();
    }

    void Swap(Any& rhs) {
        std::swap(rhs.inner_, inner_);
    }
    
    template<class T>
    const T& GetValue() const {
        return *dynamic_cast<Inner<T>&>(*inner_);
    }

    class InnerBase {
    public:
        virtual ~InnerBase() {}
        virtual InnerBase *Clone() const = 0;
    };

    template <class T>
    class Inner : public InnerBase {
    public:

        Inner(T value) : value_(std::move(value)) {}

        virtual InnerBase *Clone() const override {
            return new Inner<T>(value_);
        }

        T& operator*(){
            return value_;
        }

        const T& operator*() const {
            return value_;
        }
    private:
        T value_;
    };

private:

    std::unique_ptr<InnerBase> inner_;
};
