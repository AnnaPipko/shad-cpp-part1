#pragma once

#include <stdexcept>

int BinPow(int a, int64_t b, int c) {
    int64_t res = 1;
    int64_t pow = a;
    while (b > 0) {
        if (b % 2 == 1) {
            res = (res * pow) % c;
        }
        pow = (pow * pow) % c;
        b /= 2;
    }
    return res;
}
