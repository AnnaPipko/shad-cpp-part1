#pragma once

#include <boost/signals2.hpp>
#include <string>
#include <vector>
#include <unordered_map>
#include <memory>

class Client{
public:
    explicit Client(const std::string& name) : name_(name) {};

    virtual ~Client() {
        Disconnect();
    };

    std::string GetName() {
        return name_;
    }

    void SetConnection(boost::signals2::connection connection){
        connection_ = connection;
        block_ = boost::signals2::shared_connection_block(connection, false);
    }

    void Disconnect(){
        connection_.disconnect();
    }

    void Mute(){
        block_.block();
    }

    void Unmute(){
        block_.unblock();
    }

    virtual void GetMessage(const std::string& from, const std::string& message) = 0;

protected:
    std::string name_;
    boost::signals2::connection connection_;
    boost::signals2::shared_connection_block block_;
};

class Server{
public:
    void Kick(const std::string& client_name){
        clients_[client_name].disconnect();
    }

    void Write (const std::string& from, const std::string& message){
        signal_(from, message);
    }

    boost::signals2::connection Connect(Client * client) {
        boost::signals2::connection connection =
                signal_.connect(boost::bind(&Client::GetMessage, client, _1, _2));
        clients_[client->GetName()] = connection;
        return connection;
    }

private:
    boost::signals2::signal<void (std::string, std::string)> signal_;
    std::unordered_map<std::string, boost::signals2::connection> clients_;
};



class GetAllClient : public Client{
public:

    GetAllClient(const std::string& name) : Client(name) {};

    void GetMessage(const std::string& from, const std::string& message) override {
        messages_.emplace_back(from, message);
    }

    std::vector<std::pair<std::string, std::string>> GetMessages(){
        return messages_;
    }

private:
    std::vector<std::pair<std::string, std::string>> messages_;
};

class FilterClient : public Client{
public:
    FilterClient(const std::string& name, const std::string& from) : Client(name), from_(from) {};

    void GetMessage(const std::string& from, const std::string& message) override {
        if (from == from_) {
            messages_.push_back(message);
        }
    }

    std::vector<std::string> GetMessages(){
        return messages_;
    }

private:
    std::string from_;
    std::vector<std::string> messages_;
};

class ClientFactory{
public:

    ClientFactory() = default;

    void Kick(const std::string& client_name){
        server_.Kick(client_name);
    }

    void Write (const std::string& from, const std::string& message){
        server_.Write(from, message);
    }

    std::unique_ptr<Client> CreateClient(const std::string& name, const std::string& from){
        std::unique_ptr<Client> client;
        if (from.size() == 0){
            client = std::unique_ptr<Client>(new GetAllClient(name));
        } else {
            client = std::unique_ptr<Client>(new FilterClient(name, from));
        }
        auto connection = server_.Connect(client.get());
        client->SetConnection(connection);
        return client;
    }

private:
    Server server_;
};

