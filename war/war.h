#pragma once

#include <array>
#include <stdexcept>
#include <queue>


enum Winner {
    FIRST,
    SECOND,
    NONE
};

struct GameResult {
    Winner winner;
    int turn;
};

bool win(int first, int second){
    if (first == 0 && second == 9){
        return true;
    }
    if (first == 9 && second == 0){
        return false;
    }
    if (first > second){
        return true;
    }
    return false;
}

const int max_iterations = 1000000;

GameResult SimulateWarGame(const std::array<int, 5>& first_deck, const std::array<int, 5>& second_deck) {
    std::queue<int> first, second;
    for (auto el:first_deck){
        first.push(el);
    }
    for (auto el:second_deck){
        second.push(el);
    }
    int iteration = 0;
    while ((!first.empty()) && (!second.empty()) && (iteration < max_iterations)){
        int first_card = first.front();
        int second_card = second.front();
        first.pop();
        second.pop();
        if (win(first_card, second_card)){
            first.push(first_card);
            first.push(second_card);
        } else {
            second.push(first_card);
            second.push(second_card);
        }
        ++iteration;
    }
    if (first.empty()){
        return {SECOND, iteration};
    }
    if (second.empty()){
        return {FIRST, iteration};
    }
    return {NONE, iteration};
}

