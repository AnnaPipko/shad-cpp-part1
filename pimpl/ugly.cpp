#include <bad.h>
#include <ugly.h>
#include <iostream>

class SplineImpl{
public:
    SplineImpl(const std::vector<double>& x, const std::vector<double>& y, double a, double b) :
            x_(x), y_(y), a_(a), b_(b) {}

    double Interpolate(double x){
        std::vector<double> y2(x_.size(), 0);
        double y;
        mySplineSnd(x_.data(), y_.data(), x_.size(), a_, b_, y2.data());
        mySplintCube(x_.data(), y_.data(), y2.data(), x_.size(), x, &y);
        return y;
    }

private:
    std::vector<double> x_;
    std::vector<double> y_;
    double a_;
    double b_;

};

Spline::Spline(const std::vector<double>& x, const std::vector<double>& y, double a, double b) :
        Impl_(std::make_shared<SplineImpl>(x, y, a, b)) {}

double Spline::Interpolate(double x){
    return Impl_->Interpolate(x);
}