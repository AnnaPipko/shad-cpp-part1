cmake_minimum_required(VERSION 2.8)
project(pimpl)

set(SRC ugly.cpp)

if (TEST_SOLUTION)
  include_directories(../private/pimpl)
  set(SRC ../private/pimpl/ugly.cpp)
endif()

include(../common.cmake)


if (ENABLE_PRIVATE_TESTS)
  
endif()

add_executable(test_pimpl
  ${SRC}
  good.cpp
  ../commons/catch_main.cpp)
