#pragma once

class UniquePtr {
public:
    // put all required methods and operators here

    // see http://en.cppreference.com/w/cpp/memory/unique_ptr
    // and test.cpp

    UniquePtr(){
        ptr_ = nullptr;
    };

    explicit UniquePtr(TestClass* ptr) : ptr_(ptr){}

    UniquePtr(UniquePtr&& ptr){
        ptr_ = ptr.ptr_;
        ptr.ptr_ = nullptr;
    };

    UniquePtr(const UniquePtr&) = delete;

    ~UniquePtr(){
        delete ptr_;
    };

    UniquePtr& operator=(UniquePtr&& ptr){
        if(ptr_ != nullptr) {
            delete ptr_;
        }
        ptr_ = ptr.ptr_;
        ptr.ptr_ = nullptr;
        return *this;
    };

    UniquePtr& operator=(const UniquePtr&) = delete;

    TestClass* release(){
        auto ptr = ptr_;
        ptr_ = nullptr;
        return ptr;
    }

    void reset(TestClass* ptr = nullptr){
        auto old_ptr = ptr_;
        ptr_ = ptr;
        if(old_ptr != nullptr) {
            delete old_ptr;
        }
    }

    TestClass& operator*() const {
        return *ptr_;
    }

    TestClass* operator->() const {
        return ptr_;
    }



private:
    TestClass* ptr_;
};