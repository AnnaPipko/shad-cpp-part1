#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <memory>

bool IsSymbol(int c) {
    char ch = static_cast<char>(c);
    if (ch == '(' || ch == ')' || ch == '+' || ch == '-' || ch == '*' || ch == '/') {
        return true;
    }
    return false;
}

class Tokenizer {
public:
    explicit Tokenizer(std::istream* in) : in_(in) {
        Consume();
    }

    enum TokenType {
        UNKNOWN,
        NUMBER,
        SYMBOL,
        END
    };

    void Consume() {

        (*in_) >> std::ws;
        int ch = in_->peek();
        if (ch == EOF) {
            type_ = END;
            return;
        }
        if (std::isdigit(ch)){
            type_ = NUMBER;
            (*in_) >> number_;
            return;
        }
        if (IsSymbol(ch)){
            type_ = SYMBOL;
            (*in_) >> symbol_;
            return;
        }
        type_ = UNKNOWN;
    }

    TokenType GetType() {
        return type_;
    }

    int64_t GetNumber() {
        return number_;
    }

    char GetSymbol() {
        return symbol_;
    }

private:
    std::istream* in_;

    TokenType type_ = TokenType::UNKNOWN;
    int64_t number_;
    char symbol_;
};

class Expression {
public:
    virtual ~Expression() {}
    virtual int64_t Evaluate() = 0;
};

class Const : public Expression{
public:
    explicit Const(int64_t value) : value_(value) {};
    int64_t Evaluate() override{
        return value_;
    }

private:
    int64_t value_;
};

class Sum : public Expression{
public:
    Sum(std::unique_ptr<Expression>& first, std::unique_ptr<Expression>& second) :
        first_(std::move(first)), second_(std::move(second)) {};
    int64_t Evaluate() override{
        return first_->Evaluate() + second_->Evaluate();
    }

private:
    std::unique_ptr<Expression> first_;
    std::unique_ptr<Expression> second_;
};

class Minus : public Expression{
public:
    Minus(std::unique_ptr<Expression>& first, std::unique_ptr<Expression>& second) :
        first_(std::move(first)), second_(std::move(second)) {};
    int64_t Evaluate() override{
        return first_->Evaluate() - second_->Evaluate();
    }

private:
    std::unique_ptr<Expression> first_;
    std::unique_ptr<Expression> second_;
};

class Mult : public Expression{
public:
    Mult(std::unique_ptr<Expression>& first, std::unique_ptr<Expression>& second) :
        first_(std::move(first)), second_(std::move(second)) {};
    int64_t Evaluate() override{
        return first_->Evaluate() * second_->Evaluate();
    }

private:
    std::unique_ptr<Expression> first_;
    std::unique_ptr<Expression> second_;
};

class Div : public Expression{
public:
    Div(std::unique_ptr<Expression>& first, std::unique_ptr<Expression>& second) :
        first_(std::move(first)), second_(std::move(second)) {};
    int64_t Evaluate() override{
        return first_->Evaluate() / second_->Evaluate();
    }

private:
    std::unique_ptr<Expression> first_;
    std::unique_ptr<Expression> second_;
};

class UnaryMinus : public Expression{
public:
    explicit UnaryMinus(std::unique_ptr<Expression>& first) :
        first_(std::move(first)) {};
    int64_t Evaluate() override{
        return - first_->Evaluate();
    }

private:
    std::unique_ptr<Expression> first_;
};

// Expression ::= Term {('+' | '-') Term}
// Term ::= Factor {('*' | '/') Factor}
// Factor ::= Number | '(' Expression ')' | '-' Factor

std::unique_ptr<Expression> ParseTerm(Tokenizer* tok);
std::unique_ptr<Expression> ParseExpression(Tokenizer* tok);

std::unique_ptr<Expression> ParseFactor(Tokenizer* tok) {
    auto current_token_type = tok->GetType();
    if (current_token_type == Tokenizer::TokenType::NUMBER){
        auto node = std::unique_ptr<Expression>(new Const(tok->GetNumber()));
        tok->Consume();
        return node;
    }
    if (current_token_type == Tokenizer::TokenType::SYMBOL){
        if (tok->GetSymbol() == '('){
            tok->Consume();
            auto node = ParseExpression(tok);
            tok->Consume();
            return node;
        }
        if (tok->GetSymbol() == '-'){
            tok->Consume();
            auto node = ParseFactor(tok);
            return std::unique_ptr<UnaryMinus>(new UnaryMinus(node));
        }
    }
    return nullptr;
}

std::unique_ptr<Expression> ParseTerm(Tokenizer* tok) {
    auto node = ParseFactor(tok);
    while (tok->GetType() == Tokenizer::TokenType::SYMBOL) {
        auto current_token_char = tok->GetSymbol();
        if (current_token_char == '*') {
            tok->Consume();
            auto new_node = ParseFactor(tok);
            node = std::unique_ptr<Mult>(new Mult(node, new_node));
        } else {
            if (current_token_char == '/') {
                tok->Consume();
                auto new_node = ParseFactor(tok);
                node = std::unique_ptr<Div>(new Div(node, new_node));
            } else {
                break;
            }
        }
    }
    return node;
}

std::unique_ptr<Expression> ParseExpression(Tokenizer* tok) {
    auto node = ParseTerm(tok);
    while (tok->GetType() == Tokenizer::TokenType::SYMBOL) {
        auto current_token_char = tok->GetSymbol();
        if (current_token_char == '+') {
            tok->Consume();
            auto new_node = ParseTerm(tok);
            node = std::unique_ptr<Sum>(new Sum(node, new_node));
        } else {
            if (current_token_char == '-') {
                tok->Consume();
                auto new_node = ParseTerm(tok);
                node = std::unique_ptr<Minus>(new Minus(node, new_node));
            } else {
                break;
            }
        }
    }
    return node;
}
