#pragma once

#include <list>
#include <string>
#include <unordered_map>

struct Node{
    Node() {}

    Node(const std::string& new_value, const std::list<std::string>::iterator it) :
        value(new_value), list_pointer(it) {}

    std::string value;
    std::list<std::string>::iterator list_pointer;
};

class LruCache {
public:
    LruCache(size_t max_size) : max_size_(max_size) {}

    void Set(const std::string& key, const std::string& value) {
        if (map_.size() >= max_size_){
            map_.erase(list_.front());
            list_.pop_front();
        }
        list_.emplace_back(key);
        auto list_pointer = list_.end();
        --list_pointer;
        map_[key] = Node(value, list_pointer);
    }

    bool Get(const std::string& key, std::string* value) {
        auto search = map_.find(key);
        if (search == map_.end()){
            return false;
        }
        auto node = search->second;
        *value = node.value;
        list_.erase(node.list_pointer);
        list_.emplace_back(key);
        auto list_pointer = list_.end();
        --list_pointer;
        map_[key].list_pointer = list_pointer;
        return true;
    }

private:
    size_t max_size_;
    std::unordered_map<std::string, Node> map_;
    std::list<std::string> list_;
};
