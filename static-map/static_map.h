#pragma once

#include <vector>
#include <string>
#include <utility>
#include <algorithm>

bool compare_pair(std::pair<std::string, std::string> first, std::pair<std::string, std::string> second){
    return first.first < second.first;
}

class StaticMap {
public:
    explicit StaticMap(const std::vector<std::pair<std::string, std::string>>& items) : data_(items) {
        std::sort(data_.begin(), data_.end(), compare_pair);
    }

    bool Find(const std::string& key, std::string* value) const {
        auto search_result = std::lower_bound(data_.begin(), data_.end(), std::make_pair(key, ""), compare_pair);
        if (search_result == data_.end()){
            return false;
        }
        if (search_result->first == key){
            *value = search_result->second;
            return true;
        }
        return false;
    }

private:
    std::vector<std::pair<std::string, std::string>> data_;
};

