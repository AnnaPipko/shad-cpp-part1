#pragma once

#include "dungeon.h"

#include <stdexcept>
#include <vector>
#include <string>
#include <memory>
#include <queue>
#include <unordered_set>

bool CheckRoom(Room* room,
               std::vector<std::string>& keys,
               std::unordered_set<Door*>& closed_doors,
               std::unordered_set<Room*>& opened_rooms,
               std::queue<Room*>& room_queue){
    if (room->IsFinal()){
        return true;
    }
    for (size_t door_id = 0; door_id < room->NumDoors(); ++door_id){
        Door *door = room->GetDoor(door_id);
        if (door->IsOpen()) {
            Room *next_room = door->GoThrough();
            if (opened_rooms.find(next_room)==opened_rooms.end()){
                opened_rooms.insert(next_room);
                room_queue.push(next_room);
            }
        } else {
            for (auto key : keys){
                if (door->TryOpen(key)){
                    Room *next_room = door->GoThrough();
                    if (opened_rooms.find(next_room)==opened_rooms.end()){
                        opened_rooms.insert(next_room);
                        room_queue.push(next_room);
                    }
                    break;
                }
            }
            if (!door->IsOpen()) {
                closed_doors.insert(door);
            }
        }
    }
    for (size_t key_id = 0; key_id < room->NumKeys(); ++key_id){
        auto key = room->GetKey(key_id);
        keys.push_back(key);
        for (auto it = closed_doors.begin(); it != closed_doors.end(); ) {
            if ((*it)->TryOpen(key)) {
                Room *next_room = (*it)->GoThrough();
                if (opened_rooms.find(next_room)==opened_rooms.end()){
                    opened_rooms.insert(next_room);
                    room_queue.push(next_room);
                }opened_rooms.insert((*it)->GoThrough());
                it = closed_doors.erase(it);
            } else {
                ++it;
            }
        }
    }
    return false;
}

Room* FindFinalRoom(Room* starting_room) {
    std::vector<std::string> keys;
    std::unordered_set<Door*> closed_doors;
    std::unordered_set<Room*> opened_rooms;
    std::queue<Room*> room_queue;
    opened_rooms.insert(starting_room);
    room_queue.push(starting_room);
    Room *room;
    while(!room_queue.empty()){
        room = room_queue.front();
        room_queue.pop();
        if (CheckRoom(room, keys, closed_doors, opened_rooms, room_queue)) {
            return room;
        }
    }
    return nullptr;
}

