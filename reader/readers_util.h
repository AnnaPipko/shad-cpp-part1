#pragma once

#include <vector>
#include <memory>
#include <algorithm>

#include "reader.h"

class LimitReader : public Reader {
public:
    LimitReader(std::unique_ptr<Reader> reader, size_t limit) : 
        reader_(std::move(reader)), limit_(limit), pos_(0) {}

    virtual size_t Read(char* buf, size_t len) override {
        size_t read_len = std::min(len, limit_ - pos_);
        size_t result = reader_->Read(buf, read_len);
            pos_ += result;
        return result;
    }

private:
    std::unique_ptr<Reader> reader_;
    size_t limit_;
    size_t pos_;
};

class TeeReader : public Reader {
public:
    TeeReader(std::vector<std::unique_ptr<Reader>> readers) : readers_(std::move(readers)) {}

    virtual size_t Read(char* buf, size_t len) override {
        size_t result = 0;
        for (auto const& reader : readers_) {
            result += reader->Read(buf + result, len - result);
            if (result == len) {
                break;
            }
        }
        return result;
    }

private:
    std::vector<std::unique_ptr<Reader>> readers_;
};

char hex_to_char(char first, char second) {
    const char zero ='0';
    const char a = 'a';
    int result = 0;
    if (first < a) {
        result += (first - zero) * 16;
    } else {
        result += (first - a + 10) * 16;
    }
    if (second < a) {
        result += second - zero;
    } else {
        result += second - a + 10;
    }

    return static_cast<char>(result);
}

class HexDecodingReader : public Reader {
public:
    HexDecodingReader(std::unique_ptr<Reader> reader) :reader_(std::move(reader)) {}

    virtual size_t Read(char* buf, size_t len) override {
        size_t read_len = 2 * len;
        std::string chunk;
        chunk.resize(read_len);
        size_t result = reader_->Read(&chunk[0], read_len) / 2;
        for(size_t i = 0; i < result; ++i) {
            buf[i] = hex_to_char(chunk[2 * i], chunk[2 * i + 1]);
        }
        return result;
    }

private:
    std::unique_ptr<Reader> reader_;
};

