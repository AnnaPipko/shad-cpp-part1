#pragma once

#include <vector>
#include <algorithm>
#include <stdexcept>

struct Student {
    std::string name, surname;
    int year, month, day;

    Student(std::string new_name, std::string new_surname, int new_year, int new_month, int new_day) :
        name(new_name), surname(new_surname), year(new_year), month(new_month), day(new_day) {};
};

enum class SortType {
    BY_NAME,
    BY_DATE
};

bool CompareByDate(const Student& first, const Student& second) {
    if (first.year < second.year) {
        return true;
    }
    if (first.year == second.year) {
        if (first.month < second.month) {
            return true;
        }
        if (first.month == second.month) {
            if (first.day < second.day) {
                return true;
            }
            if (first.day == second.day) {
                if (first.surname.compare(second.surname) < 0) {
                    return true;
                }
                if (first.surname.compare(second.surname) == 0) {
                    if (first.name.compare(second.name) < 0) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool CompareByName(const Student& first, const Student& second) {
    if (first.surname.compare(second.surname) < 0) {
        return true;
    }
    if (first.surname.compare(second.surname) == 0) {
        if (first.name.compare(second.name) < 0) {
            return true;
        }
        if (first.name.compare(second.name) == 0) {
            if (first.year < second.year) {
                return true;
            }
            if (first.year == second.year) {
                if (first.month < second.month) {
                    return true;
                }
                if (first.month == second.month) {
                    if (first.day < second.day) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

void SortStudents(std::vector<Student> *students, SortType sort_type) {
    switch (sort_type) {
      case SortType::BY_DATE: {
        std::sort(students->begin(), students->end(), CompareByDate);
        break;
      }
      case SortType::BY_NAME: {
        std::sort(students->begin(), students->end(), CompareByName);
        break;
      }
    }
}

