#pragma once

#include <algorithm>
#include <iostream>

class ListHook {
public:
    ListHook() : prev_(this), next_(this) {};

    bool IsLinked() const {
        return prev_ != this;
    };

    void Unlink() {
        if (IsLinked()) {
            prev_->next_ = next_;
            next_->prev_ = prev_;
            prev_ = this;
            next_ = this;
        }
    };

    // Must unlink element from list
    ~ListHook() {
        Unlink();
    };

    ListHook(const ListHook&) = delete;

private:
    template <class T>
    friend class List;

    // that helper function might be useful
    void LinkBefore(ListHook* other) {
        prev_ = other->prev_;
        next_ = other;
        other->prev_->next_ = this;
        other->prev_ = this;
    };

    ListHook *prev_;
    ListHook *next_;
};

template <typename T>
class List {
public:
    class Iterator : public std::iterator<std::bidirectional_iterator_tag, T> {
    public:

        Iterator(T *current): current_(current) {}
        Iterator() {}

        Iterator& operator++() {
            current_ = static_cast<T*>(current_->next_);
            return *this;
        }

        Iterator operator++(int) {
            Iterator cpy(*this);
            current_ = static_cast<T*>(current_->next_);
            return cpy;
        }

        Iterator& operator--() {
            current_ = static_cast<T*>(current_->prev_);
            return *this;
        }

        Iterator operator--(int) {
            Iterator cpy(current_);
            current_ = static_cast<T*>(current_->prev_);
            return cpy;
        }

        T& operator*() const {
            return *current_;
        };

        T* operator->() const {
            return current_;
        };

        bool operator==(const Iterator& rhs) const {
            return current_ == rhs.current_;
        }

        bool operator!=(const Iterator& rhs) const {
            return current_ != rhs.current_;
        }

    private:
        T *current_;
    };

    List() {};
    List(const List&) = delete;
    List(List&& other) {
        dummy_.prev_ = other.dummy_.prev_;
        dummy_.next_ = other.dummy_.next_;
        other.dummy_.prev_ = &other.dummy_;
        other.dummy_.next_ = &other.dummy_;
    };

    // must unlink all elements from list
    ~List() {
        if (!IsEmpty()) {
            auto ptr = dummy_.next_;
            while (ptr != &dummy_) {
                auto next = ptr->next_;
                ptr->Unlink();
                ptr = next;
            }
            dummy_.next_ = &dummy_;
            dummy_.prev_ = &dummy_;
        }
    };

    List& operator=(const List&) = delete;
    List& operator=(List&& other) {
        dummy_.prev_ = static_cast<T*>(other.dummy_.prev_);
        dummy_.next_ = static_cast<T*>(other.dummy_.next_);
        other.dummy_.prev_ = &other.dummy_;
        other.dummy_.next_ = &other.dummy_;
        return *this;
    };

    bool IsEmpty() const {
        return !dummy_.IsLinked();
    };
    // that method is allowed to be O(n)
    size_t Size() const {
        if (IsEmpty()){
            return 0;
        }
        size_t list_size = 0;
        auto ptr = dummy_.next_;
        while (ptr != &dummy_) {
            ++list_size;
            ptr = ptr->next_;
        }
        return list_size;
    };

    // note that IntrusiveList doesn't own elements,
    // and never copies or moves T
    void PushBack(T* elem) {
        elem->LinkBefore(static_cast<T*>(&dummy_));
    };
    void PushFront(T* elem) {
        elem->LinkBefore(static_cast<T*>(dummy_.next_));
    };

    T& Front() {
        return *static_cast<T*>(dummy_.next_);
    };
    const T& Front() const {
        return *static_cast<T*>(dummy_.next_);
    };

    T& Back() {
        return *static_cast<T*>(dummy_.prev_);
    };
    const T& Back() const {
        return *static_cast<T*>(dummy_.prev_);
    };

    void PopBack() {
        dummy_.prev_->Unlink();
    };
    void PopFront() {
        dummy_.next_->Unlink();
    };

    Iterator Begin() {
        return Iterator(static_cast<T*>(dummy_.next_));
    };
    Iterator End() {
        return Iterator(static_cast<T*>(&dummy_));
    };

    // complexity of this function must be O(1)
    Iterator IteratorTo(T* element) const {
        return Iterator(element);
    };

private:
    ListHook dummy_;
};

template <typename T>
typename List<T>::Iterator begin(List<T>& list) {
    return list.Begin();
}

template <typename T>
typename List<T>::Iterator end(List<T>& list) {
    return list.End();
}
