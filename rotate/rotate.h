#pragma once

#include <stdexcept>
#include <vector>

void Rotate(std::vector<int> *data, size_t shift) {
    size_t length = data->size();
    std::vector<bool> shifted (length, false);
    size_t current = 0;
    while (current < length){
        int temp = (*data)[current];
        size_t next = (current + shift) % length;
        while (!shifted[next]){
            (*data)[current] = (*data)[next];
            shifted[current] = true;
            current = next;
            next = (current + shift) % length;
        }
        (*data)[current] = temp;
        shifted[current] = true;
        ++current;
    }
}

