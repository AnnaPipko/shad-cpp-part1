#pragma once

#include <string>
#include <stdexcept>
#include <algorithm>

int sum_digit(char x, char y){
    int res = x + y - '0' - '0';
    return res;
}

std::string LongSum(const std::string& a, const std::string& b) {
    std::string res;
    size_t pos = 0;
    int extra = 0;
    while (pos < a.size() && pos < b.size()){
        int sum = sum_digit(a[a.size() - pos - 1], b[b.size() - pos - 1]) + extra;
        char c = sum % 10 + '0';
        extra = sum / 10;
        res.push_back(c);
        ++pos;
    }
    if (a.size() > b.size()){
        while (pos < a.size()) {
            int sum = a[a.size() - pos - 1] - '0' + extra;
            char c = sum % 10 + '0';
            res.push_back(c);
            extra = sum / 10;
            ++pos;
        }

    } else if (b.size() > a.size()){
        while (pos < b.size()) {
            int sum = b[b.size() - pos - 1] - '0' + extra;
            char c = sum % 10 + '0';
            res.push_back(c);
            extra = sum / 10;
            ++pos;
        }
    }
    if (extra == 1){
        res.push_back('1');
    }
    std::reverse(res.begin(), res.end());
    return res;
}
