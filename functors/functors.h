#pragma once

#include <vector>
#include <algorithm>

template<class Functor>
class ReverseBinaryFunctor {
public:
    ReverseBinaryFunctor(Functor functor):functor_(functor) {};
    template<class T>
    bool operator()(T& first, T& second){
        return functor_(second, first);
    }

private:
    Functor functor_;
};

template<class Functor>
class ReverseUnaryFunctor {
public:
    ReverseUnaryFunctor(Functor functor):functor_(functor) {};
    template<class T>
    bool operator()(T& first){
        return !functor_(first);
    }

private:
    Functor functor_;

};

template<class Functor>
ReverseUnaryFunctor<Functor> MakeReverseUnaryFunctor(Functor functor) {
    return ReverseUnaryFunctor<Functor>(functor);
}

template<class Functor>
ReverseBinaryFunctor<Functor> MakeReverseBinaryFunctor(Functor functor) {
    return ReverseBinaryFunctor<Functor>(functor);
}

class Comparator{
public:
    Comparator(int *counter) : counter_(counter) {};
    template<class T>
    bool operator()(const T& first, const T& second){
        ++(*counter_);
        return first < second;
    }
private:
    int *counter_;
};

template<class Iterator>
int ComparisonsCount(Iterator first, Iterator last){
    int comparisons_count = 0;
    Comparator comp(&comparisons_count);
    std::sort(first, last, comp);
    return comparisons_count;
};

