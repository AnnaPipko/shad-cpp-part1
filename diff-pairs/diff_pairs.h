#pragma once

#include <vector>
#include <unordered_map>
int64_t CountPairs(const std::vector<int>& data, int x) {
    std::unordered_map<int64_t , int64_t> map;
    int64_t res = 0;
    for (auto el : data){
        map.insert({el, 0});
        map[el]++;
    }
    for (auto el : map){
        auto key = el.first;
        auto search = map.find(x - key);
        if (search != map.end()){
            if (x - key != key){
                res += el.second * search->second;
            } else {
                res += (el.second - 1) * el.second;
            }
        }


    }
    return res / 2;
}
