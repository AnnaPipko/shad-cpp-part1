#include <testing.h>
#include <diff_pairs.h>

namespace tests {

void TestAll() {
    StartTesting();
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
