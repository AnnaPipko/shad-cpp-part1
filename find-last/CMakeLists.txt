cmake_minimum_required(VERSION 2.8)
project(find-last)

if (TEST_SOLUTION)
  include_directories(../private/find-last)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_find_last test.cpp)
