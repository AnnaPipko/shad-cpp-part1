#pragma once

#include <stdexcept>

template<class Iterator, class T>
Iterator FindLast(Iterator first, Iterator last, const T& val) {
    Iterator it = last;
    while(it != first){
        --it;
        if ((*it) == val){
            return it;
        }
    }
    return last;
}

