#pragma once

#include <stdexcept>

template<class Iterator>
Iterator LocalMax(Iterator first, Iterator last) {
    if (first == last){
        return last;
    }
    Iterator next = first;
    next++;
    if (next == last) {
        return first;
    }
    if ((*next) < (*first)){
        return first;
    }
    Iterator prev = first;
    ++first;
    ++next;
    while (next != last){
        if ((*prev) < (*first) && (*next) < (*first)){
            return first;
        }
        ++prev;
        ++first;
        ++next;
    }
    if ((*prev) < (*first)){
        return first;
    }
    return last;
}

