#pragma once

#include <string>
#include <stdexcept>

std::vector<std::string> Split(const std::string& string, const std::string& delim = " ") {
    int last_pos = 0;
    int pos = string.find(delim);
    std::vector<std::string> res;
//    pos = string.find(delim, last_pos);
    while (pos != std::string::npos){
        res.push_back(string.substr(last_pos, pos - last_pos));
        last_pos = pos + delim.size();
        pos = string.find(delim, last_pos);
    }
    res.push_back(string.substr(last_pos, string.size() - last_pos));
    return res;
}
