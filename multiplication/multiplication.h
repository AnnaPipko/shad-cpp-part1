#pragma once

#include <stdexcept>

int64_t Multiply(int a, int b) {
    // throw std::runtime_error("Not implemented");
    return static_cast<int64_t>(a)*static_cast<int64_t>(b);
}
