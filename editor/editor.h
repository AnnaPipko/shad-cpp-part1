#pragma once

#include <string>
#include <memory>
#include <list>

class Command{
public:
    virtual ~Command() = default;
    virtual void Execute() = 0;
    virtual void Undo() = 0;
};

class TypeCommand;
class BackspaceCommand;
class ShiftLeftCommand;
class ShiftRightCommand;

class Editor {
public:
    friend class TypeCommand;
    friend class BackspaceCommand;
    friend class ShiftLeftCommand;
    friend class ShiftRightCommand;

    Editor();
    const std::string &GetText() const;
    void Type(char ch);
    void ShiftLeft();
    void ShiftRight();
    void Backspace();
    void Undo();
    void Redo();

private:
    std::string buffer_;
    size_t pos_;
    std::list<std::unique_ptr<Command>> commands_;
    std::list<std::unique_ptr<Command>>::iterator command_ptr_;
    bool undo_flag_;
    void AddCommand(Command* command);
    void InsertChar(char ch);
    void EraseChar();
    void MoveLeft();
    void MoveRight();
    char GetCurrentChar();

};

class TypeCommand : public Command{
public:
    TypeCommand(Editor* editor, char ch) : editor_(editor), ch_(ch), executed_(false) {};
    void Execute() override {
        if (!executed_) {
            editor_->InsertChar(ch_);
            executed_ = true;
        }
    }

    void Undo() override {
        if (executed_) {
            editor_->EraseChar();
            executed_ = false;
        }
    }

private:
    Editor* editor_;
    char ch_;
    bool executed_;
};

class BackspaceCommand : public Command{
public:
    BackspaceCommand(Editor* editor) : editor_(editor), executed_(false) {
        ch_ = editor_->GetCurrentChar();
    };
    void Execute() override {
        if (!executed_) {
            editor_->EraseChar();
            executed_ = true;
        }
    }

    void Undo() override {
        if (executed_) {
            editor_->InsertChar(ch_);
            executed_ = false;
        }
    }

private:
    Editor* editor_;
    char ch_;
    bool executed_;
};

class ShiftLeftCommand : public Command{
public:
    ShiftLeftCommand(Editor* editor) : editor_(editor), executed_(false) {};
    void Execute() override {
        if (!executed_) {
            editor_->MoveLeft();
            executed_ = true;
        }
    }

    void Undo() override {
        if (executed_) {
            editor_->MoveRight();
            executed_ = false;
        }
    }

private:
    Editor* editor_;
    bool executed_;
};

class ShiftRightCommand : public Command{
public:
    ShiftRightCommand(Editor* editor) : editor_(editor), executed_(false) {};
    void Execute() override {
        if (!executed_) {
            editor_->MoveRight();
            executed_ = true;
        }
    }

    void Undo() override {
        if (executed_) {
            editor_->MoveLeft();
            executed_ = false;
        }
    }

private:
    Editor* editor_;
    bool executed_;
};


Editor::Editor() : pos_(0), undo_flag_(false){

}

const std::string& Editor::GetText() const { return buffer_; }

void Editor::Type(char ch) {
    AddCommand(new TypeCommand(this, ch));
}

void Editor::ShiftLeft() {
    if (pos_ > 0){
        AddCommand(new ShiftLeftCommand(this));
    }
}

void Editor::ShiftRight() {
    if (pos_ < buffer_.size()){
        AddCommand(new ShiftRightCommand(this));
    }

}

void Editor::Backspace() {
    if (pos_ > 0){
        AddCommand(new BackspaceCommand(this));
    }
}

void Editor::Undo() {
    undo_flag_ = true;
    if (commands_.size() != 0){
        (*command_ptr_)->Undo();
        if (command_ptr_ != commands_.begin()){
            --command_ptr_;
        }
    }
}

void Editor::Redo() {
    if (commands_.size() != 0) {
        auto last = commands_.end();
        --last;
        if (command_ptr_ != last) {
            ++command_ptr_;
            (*command_ptr_)->Execute();
        }
    }
}


void Editor::AddCommand(Command* command){
    if (undo_flag_){
        commands_.clear();
        undo_flag_ = false;
    }
    commands_.push_back(std::unique_ptr<Command>(command));
    command_ptr_ = commands_.end();
    --command_ptr_;
    (*command_ptr_)->Execute();
}

void Editor::InsertChar(char ch) {
    if (pos_ == buffer_.size()){
        buffer_.push_back(ch);
    } else {
        buffer_.insert(pos_, 1, ch);
    }
    ++pos_;
}

void Editor::EraseChar(){
    if (pos_ > 0){
        --pos_;
        buffer_.erase(pos_, 1);
    }
}

void Editor::MoveLeft(){
    if (pos_ > 0) {
        --pos_;
    }
}

void Editor::MoveRight(){
    if (pos_ < buffer_.size()) {
        ++pos_;
    }
}

char Editor::GetCurrentChar(){
    return buffer_[pos_ - 1];
}

