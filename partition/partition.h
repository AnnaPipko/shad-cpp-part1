#pragma once

#include <stdexcept>

template<class Iterator, class Predicate>
Iterator Partition(Iterator first, Iterator last, Predicate pred) {
    while (first != last && pred(*first)) {
        ++first;
    }
    if (first == last){
        return last;
    }
    auto end = last;
    end--;
    while (end != first && !pred(*end)){
        --end;
    }
    if (end == first){
        return first;
    }
    std::swap(*first, *end);
    while(first != end){
        if (pred(*first)) {
            ++first;
        } else {
            if (!pred(*end)) {
                --end;
            } else {
                std::swap(*first, *end);
            }
        }
    }
    return first;
}


