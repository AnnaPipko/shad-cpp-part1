cmake_minimum_required(VERSION 2.8)
project(tryhard)

if (TEST_SOLUTION)
  include_directories(../private/tryhard)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_tryhard
  test.cpp
  ../commons/catch_main.cpp)
