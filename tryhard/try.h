#pragma once

#include <exception>
#include <stdexcept>
#include <memory>
#include <cstring>

#include <iostream>

template<class T>
class Try{
public:
    Try() {}

    Try(const T& value) : value_(new T(value)){}

    Try(std::exception_ptr eptr) {
        try {
            if (eptr) {
                std::rethrow_exception(eptr);
            }
        } catch(const std::exception& e) {
            what_ = std::string(e.what());
            exc_ = std::current_exception();
        }
    }

    Try(const std::exception& e) : what_(e.what()){}

    const T& Value() const {
        if (value_){
            return *value_;
        }
        if (exc_){
            std::rethrow_exception(exc_);
        }
        if (what_.size() > 0){
            throw std::runtime_error(what_);
        }
        throw std::runtime_error("Object is empty");
    }

    void Throw() const {
        if (exc_){
            std::rethrow_exception(exc_);
        }
        if (what_.size() > 0){
            throw std::runtime_error(what_);
        }
        throw std::runtime_error("No exception");
    }

    bool IsFailed() const {
        if (what_.size() > 0){
            return true;
        }
        return false;
    }

private:
    std::shared_ptr<T> value_;
    std::exception_ptr exc_;
    std::string what_;
};

template<>
class Try<void> {
public:
    Try() {}

    Try(std::exception_ptr eptr) {
        try {
            if (eptr) {
                std::rethrow_exception(eptr);
            }
        } catch(const std::exception& e) {
            what_ = std::string(e.what());
            exc_ = std::current_exception();
        }
    }

    Try(const std::exception& e) : what_(e.what()){}

    void Throw() const {
        if (exc_){
            std::rethrow_exception(exc_);
        }
        if (what_.size() > 0){
            throw std::runtime_error(what_);
        }
        throw std::runtime_error("No exception");
    }

    bool IsFailed() const {
        if (what_.size() > 0){
            return true;
        }
        return false;
    }


private:
    std::exception_ptr exc_;
    std::string what_;
};

template<class ReturnType>
class TryRunClass{
public:
    template<class Function, class... Args>
    static Try<ReturnType> TryRun(Function func, Args... args){
        return Try<ReturnType>(func(args...));
    }
};

template<>
class TryRunClass<void>{
public:
    template<class Function, class... Args>
    static Try<void> TryRun(Function func, Args... args){
        func(args...);
        return Try<void>();
    }
};

template<class Function, class... Args>
auto TryRun(Function func, Args... args) {
    using ReturnType = decltype(func(args...));
    try {
        return TryRunClass<ReturnType>::TryRun(func,args...);
    } catch (const std::exception& e){
        return Try<ReturnType>(std::current_exception());
    } catch (const char *what){
        return Try<ReturnType>(std::runtime_error(what));
    } catch (int err){
        return Try<ReturnType>(std::runtime_error(std::strerror(err)));
    } catch(...){
        return Try<ReturnType>(std::runtime_error("Unknown exception"));
    }
}
