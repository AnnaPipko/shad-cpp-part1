# Reallol

Это задача типа [crashme](https://best-cpp-course-ever.ru/prime/shad-cpp/blob/master/crash_readme.md).

Исходный код находится в файле `main.cpp`. Исполяемый файл получен командой
```
g++ main.cpp -o /tmp/a.out -std=c++11 -fsanitize=address
```

Советуем не писать ввод каждый раз руками, а сохранить его в файл и
пользоваться перенаправлением ввода
```
./reallol <input
```

Послать ввод из файла на сервер можно командой:
```
(echo reallol; sleep 1; cat input) | nc crashme.best-cpp-course-ever.ru 80
```
