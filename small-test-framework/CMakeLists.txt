cmake_minimum_required(VERSION 2.8)
project(list)

if (TEST_SOLUTION)
  include_directories(../private/small-test-framework)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_test
  test.cpp
  ../commons/catch_main.cpp)
