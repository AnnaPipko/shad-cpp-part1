#pragma once

#include <string>
#include <map>

class AbstractTest {
public:
    virtual void SetUp() = 0;
    virtual void TearDown() = 0;
    virtual void Run() = 0;
    virtual ~AbstractTest() {}
};

class Fabric{
public:
    virtual ~Fabric(){};
    virtual std::unique_ptr<AbstractTest> CreateTest() = 0;
};

template <class TestClass>
class TestFabric : public Fabric{
public:
    std::unique_ptr<AbstractTest> CreateTest() override {
        return std::make_unique<TestClass>();
    }
};

class TestRegistry {
public:

    TestRegistry(const TestRegistry&) = delete;
    TestRegistry(TestRegistry&&) = delete;
    TestRegistry& operator=(const TestRegistry&) = delete;
    TestRegistry& operator=(TestRegistry&&) = delete;

    static TestRegistry& Instance(){
        static TestRegistry instance;
        return instance;
    }

    template<class TestClass>
    void RegisterClass(const std::string& class_name) {
        fabrics_[class_name] = std::make_unique<TestFabric<TestClass>>();
    }

    std::unique_ptr<AbstractTest> CreateTest(const std::string& class_name) {
        if (fabrics_.find(class_name) != fabrics_.end()) {
            return fabrics_[class_name]->CreateTest();
        }
        throw std::out_of_range("No such test");
    }

    void RunTest(const std::string& test_name) {
        auto test = CreateTest(test_name);
        test->SetUp();
        try{
            test->Run();
        } catch (...){
            test->TearDown();
            std::rethrow_exception(std::current_exception());
        }
        test->TearDown();
    }

    template<class Predicate>
    std::vector<std::string> ShowTests(Predicate callback) const {
        std::vector<std::string> res;
        for (auto it = fabrics_.begin(); it != fabrics_.end(); ++it){
            if (callback(it->first)){
                res.push_back(it->first);
            }
        }
        std::sort(res.begin(), res.end());
        return res;
    }

    std::vector<std::string> ShowAllTests() const {
        std::vector<std::string> res;
        for (auto it = fabrics_.begin(); it != fabrics_.end(); ++it) {
            res.push_back(it->first);
        }
        std::sort(res.begin(), res.end());
        return res;
    }

    template<class Predicate>
    void RunTests(Predicate callback) {
        for (auto it = fabrics_.begin(); it != fabrics_.end(); ++it) {
            if (callback(it->first)){
                RunTest(it->first);
            }
        }
    }

    void Clear() {
        fabrics_.clear();
    }

private:
    TestRegistry() {};
    std::map<std::string, std::unique_ptr<Fabric>> fabrics_;
};

class FullMatch{
public:
    explicit FullMatch(std::string pattern) : pattern_(pattern) {};
    bool operator()(const std::string& str){
        return pattern_== str;
    }

private:
    std::string pattern_;
};

class Substr{
public:
    explicit Substr(std::string pattern) : pattern_(pattern) {};
    bool operator()(const std::string& str){
        return str.rfind(pattern_) != std::string::npos;
    }

private:
    std::string pattern_;
};
