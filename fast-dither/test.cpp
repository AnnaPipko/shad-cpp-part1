#include <catch.hpp>
#include <util.h>

#include <dither.h>
#include <cmath>

TEST_CASE("Read image", "[image]") {
    Image small("../small.png");

    REQUIRE(small.Height() == 512);
    REQUIRE(small.Width() == 512);
    {
        RGB expected{223, 137, 133};
        REQUIRE(small.GetPixel(2, 2) == expected);
    }
    {
        RGB expected{208, 109, 108};
        REQUIRE(small.GetPixel(0, 256) == expected);
    }
    {
        RGB expected{200, 99, 90};
        REQUIRE(small.GetPixel(1, 511) == expected);
    }
    {
        RGB expected{208, 109, 108};
        REQUIRE(!(small.GetPixel(2, 2) == expected));
        small.SetPixel(expected, 2, 2);
        REQUIRE(small.GetPixel(2, 2) == expected);
    }
}

TEST_CASE("Write image", "[image]") {
    Image small("../small.png");
    small.Write("../small_copy.png");
    Image small_copy("../small_copy.png");
    REQUIRE(small.GetPixel(2, 2) == small_copy.GetPixel(2, 2));
    REQUIRE(small.GetPixel(223, 117) == small_copy.GetPixel(223, 117));
    REQUIRE(small.GetPixel(364, 99) == small_copy.GetPixel(364, 99));
}

TEST_CASE("Write red image", "[image]") {
    Image small("../small.png");
    RGB red{255, 0 , 0};
    for (int i = 0; i < small.Height(); ++i){
        for (int j = 0; j < small.Width(); ++j){
            small.SetPixel(red, i, j);
        }
    }
    REQUIRE(small.GetPixel(2, 2) == red);
    small.Write("../small_red.png");
}

TEST_CASE("Kd tree", "[tree]") {
    {
        Points points{
            {0, 0, 0},
            {1, 2, 3},
            {1, 1, 0}
        };
        KdTree tree(points);
        std::vector<double> pt{1, 1, 1};
        REQUIRE(tree.GetNearest(pt) == 2u);
    }
    {
        Points points;
        const int n = 3e4;
        points.reserve(n);
        for (int i = 0; i < n; ++i) {
            double val = i;
            std::vector<double> cur{val, val, 0.0};
            points.push_back(cur);
        }
        KdTree tree(points);
        RandomGenerator rnd(47875656);
        for (int i = 0; i < n; ++i) {
            auto pt = rnd.GenRealVector(3, 0, n);
            int ind = tree.GetNearest(pt);
            double b = pt[0] + pt[1];
            double x = b / 2.0;
            int expected = static_cast<int>(round(x));
            expected = std::min(expected, n - 1);
            REQUIRE(ind == expected);
        }
    }
}

TEST_CASE("Full (small)", "[full]") {
    std::vector<RGB> pixels{
        RGB{0, 0, 0},
        RGB{255, 255, 255}
    };
    Dither("../small.png", "../small-result.png", pixels);
    {
        Image test("../small-result.png");
        std::vector<std::pair<int, int>> pos{
            {205, 441},
            {400, 304},
            {1, 52},
            {123, 64},
            {432, 328}
        };
        std::vector<RGB> expected{
            RGB{255, 255, 255},
            RGB{255, 255, 255},
            RGB{255, 255, 255},
            RGB{0, 0, 0},
            RGB{255, 255, 255}
        };
        for (size_t i = 0; i < pos.size(); ++i) {
            REQUIRE(test.GetPixel(pos[i].first, pos[i].second) == expected[i]);
        }
    }
}

TEST_CASE("Full (middle)", "[full]") {
    std::vector<RGB> pixels{
        {215, 198, 124},
        {174, 159, 109},
        {27, 24, 23},
        {10, 33, 55},
        {8, 9, 24},
        {8, 8, 8},
        {47, 40, 37},
        {165, 176, 172},
        {107, 89, 58},
        {177, 139, 62},
        {50, 73, 88},
        {229, 223, 186},
        {72, 60, 48},
        {102, 120, 124},
        {130, 115, 84}
    };
    Dither("../middle.png", "../middle-result.png", pixels);
    {
        Image test("../middle-result.png");
        std::vector<std::pair<int, int>> pos{
            {318, 87},
            {145, 263},
            {6, 37},
            {446, 281},
            {53, 366}
        };
        std::vector<RGB> expected{
            RGB{72, 60, 48},
            RGB{8, 8, 8},
            RGB{8, 9, 24},
            RGB{8, 8, 8},
            RGB{8, 9, 24}
        };
        for (size_t i = 0; i < pos.size(); ++i) {
            REQUIRE(test.GetPixel(pos[i].first, pos[i].second) == expected[i]);
        }
    }
}

TEST_CASE("Full (big)", "[full]") {
    std::vector<RGB> pixels;
    std::mt19937 gen(84576756);
    std::uniform_int_distribution<int> dist(0, 255);
    for (int i = 0; i < 100; ++i) {
        RGB cur{dist(gen), dist(gen), dist(gen)};
        pixels.push_back(cur);
    }
    Dither("../italian-landscape-mountains-nature.png", "../italian-landscape-mountains-nature-result.png", pixels);
}
