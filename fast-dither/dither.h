#pragma once

#include <string>
#include <vector>

#include <image.h>
#include <kd_tree.h>

struct RGB_double {
    double r, g, b;

    RGB_double() : r(0.), g(0.), b(0.){}

    RGB_double(double r_value, double g_value, double b_value) :
            r(r_value), g(g_value), b(b_value) {};

    RGB_double(int r_int, int g_int, int b_int){
        r = static_cast<double>(r_int);
        g = static_cast<double>(g_int);
        b = static_cast<double>(b_int);
    }

    RGB_double(const RGB& point){
        r = static_cast<double>(point.r);
        g = static_cast<double>(point.g);
        b = static_cast<double>(point.b);
    }

    RGB_double(std::vector<double> point){
        r = point[0];
        g = point[1];
        b = point[2];
    }

    std::vector<double> ToVector(){
        return std::vector<double>{r, g, b};
    }

    RGB_double& operator=(const RGB_double& other){
        r = other.r;
        g = other.g;
        b = other.b;
        return *this;
    }

    RGB_double operator+(const RGB_double& other){
        return {r + other.r, g + other.g, b + other.b};
    }

    RGB_double operator-(const RGB_double& other){
        return {r - other.r, g - other.g, b - other.b};
    }

    RGB_double operator*(const double factor){
        return {r * factor, g * factor, b * factor};
    }
};

void Dither(const std::string& input_file, const std::string& output_file,
            const std::vector<RGB>& ok_pixels) {

    Image image(input_file);
    int width = image.Width();
    int height = image.Height();
    std::vector<std::vector<RGB_double>> buffer;
    for (int i = 0; i < height; ++i){
        buffer.emplace_back();
        buffer.back().emplace_back();
        for (int j = 0; j < width; ++j){
            buffer.back().emplace_back(image.GetPixel(i,j));
        }
        buffer.back().emplace_back();
    }
    buffer.push_back(std::vector<RGB_double>(width + 2, RGB_double()));

    Points points;
    for (const auto& pixel : ok_pixels){
        points.push_back(RGB_double(pixel).ToVector());
    }

    auto kd_tree = KdTree(points);

    for (int i = 0; i < height; ++i){
        for (int j = 1; j < width + 1; ++j){
            RGB_double pixel = buffer[i][j];
            RGB_double new_pixel(points[kd_tree.GetNearest(pixel.ToVector())]);
            auto error = pixel - new_pixel;
            buffer[i][j] = new_pixel;
            buffer[i][j + 1] = buffer[i][j + 1] + error * (7./16);
            buffer[i + 1][j - 1] = buffer[i + 1][j - 1] + error * (3./16);
            buffer[i + 1][j] = buffer[i + 1][j] + error * (5./16);
            buffer[i + 1][j + 1] = buffer[i + 1][j + 1] + error * (1./16);
        }
    }

    for (int i = 0; i < height; ++i){
        for (int j = 1; j < width + 1; ++j){
            auto pixel = buffer[i][j];
            image.SetPixel(RGB{pixel.r, pixel.g, pixel.b}, i, j - 1);
        }
    }

    image.Write(output_file);
}
