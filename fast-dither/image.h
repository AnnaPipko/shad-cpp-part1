#pragma once

#include <png.h>
#include <iostream>
#include <memory>

struct RGB {
    int r, g, b;

    RGB(double r_double, double g_double, double b_double){
        r = static_cast<int>(round(r_double));
        g = static_cast<int>(round(g_double));
        b = static_cast<int>(round(b_double));
    }

    bool operator==(const RGB& rhs) const {
        return r == rhs.r && g == rhs.g && b == rhs.b;
    }
};

std::ostream& operator<<(std::ostream& out, const RGB& x) {
    out << x.r << " " << x.g << " " << x.b;
    return out;
}

class Image {
public:
    explicit Image(const std::string& filename) {

        png_byte color_type;
        png_byte bit_depth;

        FILE *fp = fopen(filename.c_str(), "rb");
        png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        png_infop info = png_create_info_struct(png);
        png_init_io(png, fp);
        png_read_info(png, info);
        width_ = png_get_image_width(png, info);
        height_ = png_get_image_height(png, info);
        color_type = png_get_color_type(png, info);
        bit_depth = png_get_bit_depth(png, info);
        if(bit_depth == 16) {
            png_set_strip_16(png);
        }
        if(color_type == PNG_COLOR_TYPE_PALETTE) {
            png_set_palette_to_rgb(png);
        }
        if(color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8) {
            png_set_expand_gray_1_2_4_to_8(png);
        }
        if(png_get_valid(png, info, PNG_INFO_tRNS)) {
            png_set_tRNS_to_alpha(png);
        }
        if(color_type == PNG_COLOR_TYPE_RGB ||
           color_type == PNG_COLOR_TYPE_GRAY ||
           color_type == PNG_COLOR_TYPE_PALETTE) {
            png_set_filler(png, 0xFF, PNG_FILLER_AFTER);
        }
        png_read_update_info(png, info);
        row_pointers_ = (png_bytep*)malloc(sizeof(png_bytep) * height_);
        for(int y = 0; y < height_; y++) {
            row_pointers_[y] = (png_byte*)malloc(png_get_rowbytes(png,info));
        }
        png_read_image(png, row_pointers_);
        png_destroy_read_struct(&png, &info, nullptr);
        png = nullptr;
        info = nullptr;
        fclose(fp);
    }

    ~Image(){
        for(int row = 0; row < height_; row++) {
            free(row_pointers_[row]);
        }
        free(row_pointers_);
    }

    void Write(const std::string& filename) {

        FILE *fp = fopen(filename.c_str(), "wb");
        png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        png_infop info = png_create_info_struct(png);
        png_init_io(png, fp);
        png_set_IHDR(
                png,
                info,
                width_, height_,
                8,
                PNG_COLOR_TYPE_RGB,
                PNG_INTERLACE_NONE,
                PNG_COMPRESSION_TYPE_DEFAULT,
                PNG_FILTER_TYPE_DEFAULT
        );
        png_write_info(png, info);
        png_set_filler(png, 0, PNG_FILLER_AFTER);
        png_write_image(png, row_pointers_);
        png_write_end(png, NULL);
        fclose(fp);
        if (png && info) {
            png_destroy_write_struct(&png, &info);
        }
    }

    RGB GetPixel(int y, int x) const {
        png_byte* pixel = &(row_pointers_[y][x*4]);
        return {static_cast<int>(pixel[0]), static_cast<int>(pixel[1]), static_cast<int>(pixel[2])};
    }

    void SetPixel(const RGB& pixel, int y, int x) {
        png_byte* img_pixel = &(row_pointers_[y][x*4]);
        img_pixel[0] = pixel.r;
        img_pixel[1] = pixel.g;
        img_pixel[2] = pixel.b;
    }

    int Height() const {
        return height_;
    }

    int Width() const {
        return width_;
    }

private:
    int width_, height_;
    png_bytep *row_pointers_;
};
