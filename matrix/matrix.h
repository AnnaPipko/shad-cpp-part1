#pragma once

#include <vector>

class Matrix{
public:

    Matrix(size_t n_rows, size_t n_cols){
        data_ = std::vector<std::vector<int>>(n_rows, std::vector<int>(n_cols, 0));
    }

    Matrix(size_t size) : Matrix(size,size){}

    Matrix(const std::vector<std::vector<int>>& input){
        data_ = std::vector<std::vector<int>>(input);
    }

    size_t Rows() const {
        return data_.size();
    }

    size_t Columns() const {
        if (data_.empty()){
            return 0;
        }
        return data_[0].size();
    }

    int& operator()(size_t row, size_t col){
        return data_[row][col];
    }

    const int& operator()(size_t row, size_t col) const {
        return data_[row][col];
    }

    Matrix& operator+=(const Matrix& rhs){
        for (size_t i = 0; i < Rows(); ++i){
            for (size_t j = 0; j < Columns(); ++j){
                (*this)(i,j) += rhs(i,j);
            }
        }
        return *this;
    }

    Matrix& operator-=(const Matrix& rhs){
        for (size_t i = 0; i < Rows(); ++i){
            for (size_t j = 0; j < Columns(); ++j){
                (*this)(i,j) -= rhs(i,j);
            }
        }
        return *this;
    }

    friend Matrix operator*(const Matrix& lhs, const Matrix& rhs);

    Matrix& operator*=(const Matrix& rhs){
        return *this = *this * rhs;
    }


private:
    std::vector<std::vector<int>> data_;
};

Matrix operator+(Matrix lhs, const Matrix& rhs){
    return lhs+=rhs;
}

Matrix operator-(Matrix lhs, const Matrix& rhs){
    return lhs-=rhs;
}

Matrix operator*(const Matrix& lhs, const Matrix& rhs){
    Matrix result(lhs.Rows(), rhs.Columns());
    for (size_t i = 0; i < result.Rows(); ++i){
        for (size_t j = 0; j < result.Columns(); ++j){
            int sum = 0;
            for (size_t k = 0; k < lhs.Columns(); ++k){
                sum += lhs(i,k) * rhs(k,j);
            }
            result(i,j) = sum;
        }
    }
    return result;
}

Matrix Transpose(const Matrix& matrix){
    Matrix result(matrix.Columns(), matrix.Rows());
    for (size_t i = 0; i < result.Rows(); ++i){
        for (size_t j = 0; j < result.Columns(); ++j){
            result(i,j) = matrix(j,i);
        }
    }
    return result;
}

Matrix Identity(size_t size){
    Matrix result(size);
    for (size_t i = 0; i < size; ++i){
        result(i,i) = 1;
    }
    return result;
}

