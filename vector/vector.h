#pragma once

#include <initializer_list>
#include <iterator>

template<class T>
class Vector {
public:
    Vector() : size_(0), capacity_(0), data_(nullptr) {};

    explicit Vector(size_t size) : size_(size), capacity_(size) {
        data_ = new T[size];
        for (size_t i = 0; i < size; ++i) {
            data_[i] = T();
        }
    }

    Vector(const Vector& rhs) {
        size_ = rhs.size_;
        capacity_ = size_;
        data_ = new T[size_];
        std::copy(rhs.data_, rhs.data_ + size_, data_);
    }

    Vector(Vector&& rhs) {
        size_ = rhs.size_;
        capacity_ = rhs.capacity_;
        data_ = rhs.data_;
        rhs.data_ = nullptr;
    }

    template<class Iterator>
    Vector(Iterator first, Iterator last) {
        size_ = std::distance(first, last);
        capacity_ = size_;
        data_ = new T[size_];
        size_t i = 0;
        while (first != last) {
            data_[i++] = *first;
            ++first;
        }
    }

    explicit Vector(std::initializer_list<T> list):
        Vector(list.begin(), list.end()) {}

    ~Vector() {
        delete[] data_;
    }

    void Swap(Vector& rhs) {
        std::swap(size_, rhs.size_);
        std::swap(capacity_, rhs.capacity_);
        std::swap(data_, rhs.data_);
    }

    Vector& operator=(Vector rhs) {
        Swap(rhs);
        return *this;
    }

    T& operator[](size_t ind) {
        return data_[ind];
    }

    const T& operator[](size_t ind) const {
        return data_[ind];
    }

    size_t Size() const {
        return size_;
    }

    size_t Capacity() const {
        return capacity_;
    }

    template <typename U>
    void PushBack(U&& u){
        if (capacity_ < size_ + 1){
            if (capacity_ > 0){
                capacity_ *= 2;
            } else {
                capacity_ = 1;
            }
            T *new_data = new T[capacity_];
            for (size_t i = 0; i < size_; ++i){
                new_data[i] = data_[i];
            }
            delete[] data_;
            data_ = new_data;
        }
        data_[size_] = std::forward<U>(u);
        size_++;
    }

    void PopBack(){
        size_--;
    }

    void Clear(){
        size_ = 0;
    }

    void Reserve(size_t capacity){
        if (capacity_ < capacity){
            T *new_data = new T[capacity];
            for (size_t i = 0; i < size_; ++i){
                new_data[i] = data_[i];
            }
            delete[] data_;
            data_ = new_data;
            capacity_ = capacity;
        }
    }

    class Iterator:
            public std::iterator<std::random_access_iterator_tag, T> {
    public:
        Iterator(T *current): current_(current) {}
        Iterator() {}

        T& operator*() {
            return *current_;
        }

        T* operator->() {
            return current_;
        }

        Iterator& operator++() {
            ++current_;
            return *this;
        }

        Iterator operator++(int) {
            Iterator cpy(*this);
            ++current_;
            return cpy;
        }

        Iterator& operator--() {
            --current_;
            return *this;
        }

        Iterator operator--(int) {
            Iterator cpy(*this);
            --current_;
            return cpy;
        }

        Iterator& operator+=(int n){
            if (n >= 0){
                while (n--){
                    ++(*this);
                }
            } else {
                while (n++){
                    --(*this);
                }
            }
            return *this;
        }

        Iterator& operator-=(int n){
            return (*this) += -n;
        }

        Iterator operator+(int n) const {
            Iterator temp = *this;
            return temp += n;
        }

        Iterator operator-(int n) const {
            return *this + (-n);
        }

        size_t operator-(const Iterator& rhs) const {
            return current_ - rhs.current_;
        }

        bool operator==(const Iterator& rhs) const {
            return current_ == rhs.current_;
        }

        bool operator!=(const Iterator& rhs) const {
            return current_ != rhs.current_;
        }
    private:
        T *current_;
    };

    Iterator begin() {
        return Iterator(data_);
    }

    Iterator end() {
        return Iterator(data_ + size_);
    }

    Iterator Begin() {
        return Iterator(data_);
    }

    Iterator End() {
        return Iterator(data_ + size_);
    }



private:
    size_t size_;
    size_t capacity_;
    T *data_;
};

template <class T>
typename Vector<T>::Iterator operator+(int n, const typename Vector<T>::Iterator& it){
    return it+n;
}