cmake_minimum_required(VERSION 2.8)
project(vector)

if (TEST_SOLUTION)
  include_directories(../private/vector)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_vector
  test.cpp
  ../commons/catch_main.cpp)
