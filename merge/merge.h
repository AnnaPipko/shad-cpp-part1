#pragma once

#include <stdexcept>

template<class Iterator1, class Iterator2, class OutIterator>
OutIterator Merge(Iterator1 first1, Iterator1 last1,
                  Iterator2 first2, Iterator2 last2,
                  OutIterator result) {
    while ((first1 != last1) && (first2 != last2)){
        if (*first2 < *first1){
            *result = *first2;
            ++result;
            ++first2;
        } else {
            *result = *first1;
            ++result;
            ++first1;
        }
    }
    while(first1 != last1){
        *result = *first1;
        ++result;
        ++first1;
    }
    while(first2 != last2){
        *result = *first2;
        ++result;
        ++first2;
    }
    return result;
}

