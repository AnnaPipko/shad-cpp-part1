#pragma once

#include <stdexcept>
#include <map>
#include <string>
#include <vector>
#include <tuple>
#include <algorithm>
#include <iostream>

struct StudentName {
    std::string name, surname;

    StudentName(std::string new_name, std::string new_surname) :
        name(new_name), surname(new_surname) {};

    StudentName() : name(""), surname("") {};
};

struct Date {
    int year, month, day;

    Date(int new_year, int new_month, int new_day) :
        year(new_year), month(new_month), day(new_day) {};
};

struct Student {
    StudentName name;
    Date date;
    int score;
    const std::vector<std::string>* universities;

    Student(StudentName new_name, Date new_date, int new_score, const std::vector<std::string>* universities_list) :
        name(new_name), date(new_date), score(new_score), universities(universities_list) {};
};

bool FullCompare(const Student& first, const Student& second) {
    if (first.score > second.score) {
        return true;
    }
    if (first.score == second.score) {
        if (first.date.year < second.date.year) {
            return true;
        }
        if (first.date.year == second.date.year) {
            if (first.date.month < second.date.month) {
                return true;
            }
            if (first.date.month == second.date.month) {
                if (first.date.day < second.date.day) {
                    return true;
                }
                if (first.date.day == second.date.day) {
                    if (first.name.surname.compare(second.name.surname) < 0) {
                        return true;
                    }
                    if (first.name.surname.compare(second.name.surname) == 0) {
                        if (first.name.name.compare(second.name.name) < 0) {
                            return true;
                        }
                    }
                }
            }
        }
    }
    return false;
}

bool CompareByName(const StudentName& first, const StudentName& second) {
    if (first.surname.compare(second.surname) < 0) {
        return true;
    }
    if (first.surname.compare(second.surname) == 0) {
        if (first.name.compare(second.name) < 0) {
            return true;
        }
    }
    return false;
}


std::map<std::string, std::vector<StudentName>> GetStudents(
    const std::vector<std::pair<std::string, int>>& university_info,
    const std::vector<std::tuple<StudentName, Date, int, std::vector<std::string>>>& students_info) {

    std::map<std::string, int> university_map;
    for (const auto& university : university_info) {
        university_map[university.first] = university.second;
    }

    std::map<std::string, std::vector<StudentName>> result;

    std::vector<Student> students;
    for (const auto& student : students_info) {
        students.emplace_back(std::get<0>(student), std::get<1>(student), std::get<2>(student), &std::get<3>(student));
    }

    std::sort(students.begin(), students.end(), FullCompare);
    for (auto const& student : students) {
        bool enrolled = false;
        auto university = student.universities->begin();
        while (!enrolled && university != student.universities->end()) {
            std::cout << *university;
            if (university_map[*university] > 0) {
                university_map[*university]--;
                enrolled = true;
                result[*university].emplace_back(student.name.name, student.name.surname);
            }
            ++university;
        }
    }
    for (const auto& university : university_info) {
        std::sort(result[university.first].begin(), result[university.first].end(), CompareByName);
    }
    return result;
}
