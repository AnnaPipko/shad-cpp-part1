#pragma once

#include <memory>
#include <vector>

template<class T>
std::vector<std::unique_ptr<T>> Duplicate(const std::vector<std::shared_ptr<T>>& items){
    std::vector<std::unique_ptr<T>> result;
    for (const auto& item : items){
        result.push_back(std::make_unique<T>(*(item.get())));
    }
    return result;
};

template<class T>
std::vector<std::shared_ptr<T>> DeDuplicate(const std::vector<std::unique_ptr<T>>& items){
    std::vector<std::shared_ptr<T>> result;
    for (const auto& item : items){
        bool shared = false;
        for (const auto& iter : result){
            if (!(*item < *iter) && !(*iter < *item)){
                result.push_back(iter);
                shared = true;
                break;
            }
        }
        if (!shared){
            result.push_back(std::make_shared<T>(*(item.get())));
        }
    }
    return result;
};