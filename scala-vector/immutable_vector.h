#pragma once


template<class T>
class ImmutableVector {
public:
    ImmutableVector() {}

    explicit ImmutableVector(size_t count, const T& value = T()) {}

    template<typename Iterator>
    ImmutableVector(Iterator first, Iterator last) {}

    ImmutableVector(std::initializer_list<T> l) {}

    ImmutableVector set(size_t index, const T& value) {}

    const T& get(size_t index) const {
        static T dummy_value;
        return dummy_value;
    }

    ImmutableVector push_back(const T& value) {}

    ImmutableVector pop_back() {}

    size_t size() const {
        return 0;
    }
};


