#pragma once

#include <string>
#include <cstring>

class StringView {
public:
    StringView(const std::string& str, size_t start=0, size_t len=std::string::npos){
        source_ = &str[start];
        len_ = std::min(len, str.size() - start);
    }

    StringView(const char *str) : source_(str){
        len_ = std::strlen(str);
    }

    StringView(const char *str, size_t len) : source_(str), len_(len){}

    const char& operator[](size_t i) const {
        return *(source_ + i);
    }

    size_t size() const {
        return len_;
    }

    
private:
    const char *source_;
    size_t len_;
};
