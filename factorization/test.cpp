#include <testing.h>
#include <factorization.h>
#include <vector>

using Factors = std::vector<std::pair<int64_t, int>>;

namespace tests {

void Primes() {
    {
        Factors expected{{2, 1}};
        ASSERT_EQ(expected, Factorize(2));
    }
    {
        Factors expected{{17, 1}};
        ASSERT_EQ(expected, Factorize(17));
    }
    {
        int num = 1000000007;
        Factors expected{{num, 1}};
        ASSERT_EQ(expected, Factorize(num));
    }
    {
        int64_t num = 999999999937LL;
        Factors expected{{num, 1}};
        ASSERT_EQ(expected, Factorize(num));
    }
}

void SomeTests() {
    {
        Factors expected{{17239, 2}};
        ASSERT_EQ(expected, Factorize(17239ll * 17239));
    }
    {
        Factors expected{{2, 2}, {3, 2}, {7, 2}, {101, 4}};
        ASSERT_EQ(expected, Factorize(183562547364ll));
    }
    {
        Factors expected{{3, 3}, {5, 5}, {13, 3}};
        ASSERT_EQ(expected, Factorize(185371875ll));
    }
    {
        Factors expected{{23, 4}, {100003, 1}};
        ASSERT_EQ(expected, Factorize(27984939523ll));
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(Primes);
    RUN_TEST(SomeTests);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
