cmake_minimum_required(VERSION 2.8)
project(factorization)

if (TEST_SOLUTION)
  include_directories(../private/factorization)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_factorization test.cpp)
