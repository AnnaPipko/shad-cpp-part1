#pragma once

#include <utility>
#include <vector>
#include <stdexcept>
#include <cmath>

std::vector<std::pair<int64_t, int>> Factorize(int64_t x) {
    int64_t max_divider = std::sqrt(x);
    std::vector<std::pair<int64_t, int>> result;
    int power = 0;
    while (x % 2 == 0){
        x = x / 2;
        ++power;
    }
    if (power > 0){
        result.emplace_back(2, power);
    }
    for (int64_t divider = 3; divider <= max_divider; divider += 2){
        int power = 0;
        while (x % divider == 0){
            x = x / divider;
            ++power;
        }
        if (power > 0){
            result.emplace_back(divider, power);
        }
    }
    if (result.size() == 0){
        result.emplace_back(x,1);
    }
    return result;
}
