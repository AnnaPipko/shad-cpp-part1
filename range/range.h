#pragma once

#include <vector>
#include <stdexcept>

std::vector<int> Range(int from, int to, int step=1) {
    std::vector<int> res;
    if (step == 0){
        return res;
    }
    if ((to-from) / step < 0){
        return res;
    }
    if (to == from){
        return res;
    }
    res.push_back(from);
    int el = from + step;
    if (from < to){
        while(el < to){
            res.push_back(el);
            el += step;
        }
    } else {
        while(el > to){
            res.push_back(el);
            el += step;
        }
    }

    return res;
}
